import numpy as np
import pandas as pd
import networkx as nx
import pygenstability as pgs


def save_community(number, communities, new_header, old_header):
    tmp = []
    for i, comm in enumerate(communities):
        tmp.append([new_header[i], comm])

    to_save = [[lbl, -1] for lbl in old_header]
    for i, ts in enumerate(to_save):
        lbl = ts[0]
        for t in tmp:
            if lbl in t[0]:
                ts[1] = t[1]
                break
        to_save[i] = ts
    to_save = pd.DataFrame(to_save)
    fname = 'community'+str(number+1)+'.txt'
    to_save.to_csv(fname, sep='\t', header=False, index=False)


def remove_unconnected_nodes(adjacency_matrix, header):
    # Create a graph from the adjacency matrix
    G = nx.Graph()

    for i, row in adjacency_matrix.iterrows():
        node = header[i]
        G.add_node(node)
        for column, value in row.items():
            if column != node and value == 1:
                G.add_edge(node, column)

    # Identify connected components
    components = list(nx.connected_components(G))

    if len(components) == 0:
        return np.zeros_like(adjacency_matrix)

    # Keep only the largest connected component
    largest_component = max(components, key=len)

    # Create a subgraph with the largest connected component
    G_largest = G.subgraph(largest_component)

    # Generate the new adjacency matrix
    new_adjacency_matrix = nx.to_numpy_array(G_largest)
    new_header = [n for n in G_largest.nodes]
    return G_largest, new_adjacency_matrix, new_header


def calculate_communities(adjacency_matrix, header):
    nx_graph, adjacency_matrix, new_header = remove_unconnected_nodes(
        adjacency_matrix, header)
    all_results = pgs.run(adjacency_matrix, min_scale=-1.5, max_scale=2,
                          n_scale=30, constructor='continuous_normalized')
    selected_scales = all_results["selected_partitions"]
    for i, optimal_scale_id in enumerate(selected_scales):
        communities = all_results["community_id"][optimal_scale_id]
        save_community(i, communities, new_header, header)
    
    return "Check your folder for community files"
