const margin = { top: 5, right: 200, bottom: 45, left: 60 };
const innerWidth = width - margin.left - margin.right;
const innerHeight = height - margin.top - margin.bottom;
const legendPosition = innerWidth + 10;
const legendWidth = 185;
const legendHeight = innerHeight - 70;
const spacingLegend = 25;

const bacteriaColors = options.bac_color; //d3.schemeCategory10;
let hoveredArrowToBar = options.arrow_hovered;
let selectedBullet;

const mouseOnBlock = (event, d) => {
    const bacteria = d3.select(event.currentTarget.parentNode).datum().key;
    const [mx, my] = d3.pointer(event)
    barsTooltip
        .attr('x', mx + 30)
        .attr('y', my);

    barsTooltipText
        .text(bacteria);
    const bbox = barsTooltipText.node().getBBox();
    barsTooltip.attr('width', bbox.width + 14)
        .attr('height', bbox.height + 8);
    barsTooltipText
        .attr('x', mx + 35)
        .attr('y', my + 20);
    Shiny.setInputValue(
        'bar_hovered',
        bacteria,
        { priority: 'event' }
    );
};

const mouseOutBlock = () => {
    barsTooltipText.text('');
    barsTooltip
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', 0)
        .attr('height', 0);

    Shiny.setInputValue(
        'bar_hovered',
        'null',
        { priority: 'event' }
    );
};

const onClickLegend = d => {
    selectedBullet = d;

    const legendBullets = svg.selectAll('.legend-bullet');
    legendBullets.each(function (c) {
        if (c === selectedBullet || !selectedBullet) {
            d3.select(this)
                .attr('opacity', 1)
        } else {
            d3.select(this)
                .attr('opacity', 0.2)
        }
    });

    if (!selectedBullet) {
        //loading unselected
        Shiny.setInputValue(
            'bar_cliked',
            'null',
            { priority: 'event' }
        );
    } else {
        //loading selected
        Shiny.setInputValue(
            'bar_cliked',
            selectedBullet,
            { priority: 'event' }
        );
    }
};

// this line reinitialize all the elements of the svg
svg.select('#maingroupBarPlot').remove();

const g = svg.append('g').attr('id', 'maingroupBarPlot')
    .attr('transform', `translate(${margin.left},${margin.top})`);

// Create an array of unique bacteria names for legend
const bacteriaNames = Object.keys(data[0]).filter(d => d !== 'cluster');

// Stack the data
const stacked = d3.stack()
    .keys(bacteriaNames)
const stackedData = stacked(data);
const maxValue = d3.max(stackedData, series => d3.max(series, d => d[1]));

// Define color scale
const colorScale = d3.scaleOrdinal()
    .domain(bacteriaNames)
    .range(bacteriaColors);

// Define x and y scales
const x = d3.scaleBand()
    .domain(data.map(d => d.cluster))
    .range([0, innerWidth])
    .padding(0.2);

const y = d3.scaleLinear()
    .domain([0, maxValue])
    .range([innerHeight, 0])
    .nice();

// Add x-axis
g.append('g')
    .attr('class', 'x-axis')
    .attr('transform', `translate(0,${innerHeight})`)
    .call(d3.axisBottom(x))
    .selectAll(".tick text")
    .classed("axis-text", true);

// X-axis legend
g.append('text')
    .attr('class', 'title-XAxis')
    .attr('transform', `translate(0,${innerHeight})`)
    .attr('x', innerWidth / 2)
    .attr('y', 45)
    .text('Cluster');

// Add y-axis
g.append('g')
    .attr('class', 'y-axis')
    .call(d3.axisLeft(y));

// Y-axis legend
g.append('text')
    .attr('class', 'title-YAxis')
    .attr('x', -innerHeight / 2)
    .attr('y', -48)
    .attr('transform', `rotate(-90)`)
    .attr('text-anchor', 'middle')
    .text('Relative Abundance')

// Show the bars
const barsplot = g.append('g');
barsplot.selectAll('.barplot')
    .data(stackedData)
    .enter()
    .append('g')
    .attr('class', 'barplot')
    .style('fill', d => colorScale(d.key))
    .attr('opacity', d => (d.key === hoveredArrowToBar ||
        hoveredArrowToBar === 'null' ||
        !hoveredArrowToBar) ? 1 : 0.3)
    .selectAll('rect')
    .data(d => d)
    .enter()
    .append('rect')
    .attr('x', d => x(d.data.cluster))
    .attr('y', d => y(d[1]))
    .attr('height', d => y(d[0]) - y(d[1]))
    .attr('width', x.bandwidth())
    .on('mousemove', (event, d) => mouseOnBlock(event, d))
    .on('mouseleave', mouseOutBlock);

// Legend area
const legendGroup = g.append('g')
    .attr('transform', `translate(${legendPosition},${margin.top + 50})`);

const title = legendGroup.append('g')
    .attr('class', 'title-text');

title.append('text')
    .attr('x', 10)
    .attr('y', -20)
    .text('Bacteria');

legendGroup.selectAll('.legend-bullet')
    .data(bacteriaNames)
    .enter()
    .append("rect")
    .attr('class', 'legend-bullet')
    .attr('rx', 6)
    .attr('ry', 6)
    .attr('x', 0)
    .attr("y", (d, i) => (i * spacingLegend) + 10)
    .attr("width", 20)
    .attr("height", 20)
    .style("fill", d => colorScale(d))
    .on('click', (e, d) => onClickLegend(d === selectedBullet ? null : d));



legendGroup.selectAll('.legend-items')
    .data(colorScale.domain())
    .enter()
    .append('text')
    .attr('class', 'legend-items')
    .text(d => d)
    .attr('x', 30)
    .attr('y', (d, i) => (i * spacingLegend) + 25);


const barsTooltip = g.append('rect');
barsTooltip
    .attr('class', 'rect-tooltip')
    .attr('rx', 6)
    .attr('ry', 6)
    .attr('x', 0)
    .attr('y', 0)
    .attr('fill', '#606060');

//Always at the end
const barsTooltipText = g.append('text');
barsTooltipText.attr('class', 'rect-tooltip-text')
    .attr('x', 0)
    .attr('y', 0)
    .text('');