//Axis.1        Axis.2   cluster alpha_trans          track_ID                              track_label         beds
//function utils section
const uniqueInt = (array) => {
  return array
    .filter((value, index, self) => self.indexOf(value) === index)
    .sort((a, b) => a - b); // Use a custom comparison function for sorting
}

const uniqueString = (array) => {
  return array
    .filter((value, index, self) => self.indexOf(value) === index)
}

const wrap = (text, width) => {
  text.each(function () {
    var text = d3.select(this),
      words = text.text().split(/ /).reverse(),
      word,
      line = [],
      lineNumber = 0,
      lineHeight = 1.1,
      x = text.attr('x'),
      y = text.attr('y'),
      dy = 0,
      tspan = text.text(null)
        .append('tspan')
        .attr('x', x)
        .attr('y', y)
        .attr('dy', dy + 'em');

    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(' '));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(' '));
        line = [word];
        tspan = text.append('tspan')
          .attr('x', x)
          .attr('y', y)
          .attr('dy', ++lineNumber * lineHeight + dy + 'em')
          .text(word);
      }
    }

  });
};

const calculateDistance = (point1, point2) => {
  const dx = point1.x - point2.x;
  const dy = point1.y - point2.y;
  return Math.sqrt(dx * dx + dy * dy);
};

//const variables   1          2          3          4          5          6          7          8          9          10
const colorCluster = ['#F8766D', '#00BF7D', '#619CFF', '#00BFC4', '#A3A500', '#F564E3', '#999999', '#E69F00', '#56B4E9', '#D55E00'];
const spacingLegend = 25;
const circleRadiusLegend = 6;
const scatterRadius = 4;
const colorScale = d3.scaleOrdinal(colorCluster);

const lblClusters = uniqueString(data.data_points.cluster);

const margin = { top: 5, right: 200, bottom: 45, left: 50 };
const innerWidth = width - margin.left - margin.right;
const innerHeight = height - margin.top - margin.bottom;
const legendPosition = innerWidth + 10;
const legendWidth = 185;
const legendHeight = innerHeight - 70;

//interactive variables
let selectedBullet;
let selectedSample;
let selectedLoading;
let hoveredBarToArrow = options.bar_hovered;
let arrowsVisible = true;
let trackingVisible = true;

// Mouse interactive functions
const onClickLegend = d => {
  selectedBullet = d;

  const legendBullets = svg.selectAll('.legend-bullet');
  legendBullets.each(function (c) {
    if (c === selectedBullet || !selectedBullet) {
      d3.select(this)
        .attr('opacity', 1)
    } else {
      d3.select(this)
        .attr('opacity', 0.2)
    }
  });

  const pointsclr = svg.selectAll('.points');
  pointsclr.each(function (c) {
    if (c.cluster === selectedBullet || !selectedBullet) {
      d3.select(this)
        .attr('opacity', 1)
    } else {
      d3.select(this)
        .attr('opacity', 0.2)
    }
  });

  if (!selectedBullet) { //ya capto el grupo/cluster
    //Bullet unselected
    Shiny.setInputValue(
      'lbl_cluster_cliked',
      'null',
      { priority: 'event' }
    );
  } else {
    //Bullet selected
    Shiny.setInputValue(
      'lbl_cluster_cliked',
      selectedBullet,
      { priority: 'event' }
    );
  }

};

const toggleArrowOnClick = () => {
  arrowsVisible = !arrowsVisible;

  if (arrowsVisible) {
    circleButtonLoadings.attr('fill', 'black');
  } else {
    circleButtonLoadings.attr('fill', 'white');
  }

  // // Update the visibility of the arrow lines
  loadingsplot.selectAll('.loadings')
    .style('display', arrowsVisible ? 'block' : 'none');
};

const toggleTrackingOnClick = () => {
  trackingVisible = !trackingVisible;

  if (trackingVisible) {
    circleButtonTracking.attr('fill', 'black');
  } else {
    circleButtonTracking.attr('fill', 'white');
  }

  // Update the visibility of the arrow lines
  g.selectAll('.connect-points')
    .style('display', trackingVisible ? 'block' : 'none');

  showLinesConnection();
};

const showLinesConnection = () => {
  // Remove previously added lines
  svg.selectAll('.connect-points').remove();
  if (selectedSample && trackingVisible) {
    // Filter data to get points with the same trackid
    const matchingPoints = data_points.filter(point => point.trackid === selectedSample);
    if (matchingPoints.length > 1) {
      // Create a path string for connecting the matching points
      const pathString = matchingPoints
        .map((point, i) => `${i === 0 ? 'M' : 'L'} ${xScale(point.x)},${yScale(point.y)}`)
        .join(' ');
      // Define the arrowhead marker
      g.append('defs').append('marker')
        .attr('id', 'arrowhead')
        .attr('markerWidth', 10)
        .attr('markerHeight', 10)
        .attr('refX', 5)
        .attr('refY', 5)
        .attr('orient', 'auto')
        .append('path')
        .attr('d', 'M0,0 L0,10 L10,5 Z')
        .style('fill', 'black');

      // Append the path element
      g.append('path')
        .attr('class', 'connect-points')
        .attr('d', pathString)
        .attr('fill', 'none') // No fill
        .attr('stroke', 'black')
        .attr('marker-end', 'url(#arrowhead)')
        .attr('opacity', 0.6);
    }
  }
}

const onClickTrackID = d => {

  if (d === selectedSample) {
    // If the clicked sample is the same as the currently selected sample, clear selection
    selectedSample = null;
  } else {
    selectedSample = d;
  }

  const pointsclr = svg.selectAll('.points');
  pointsclr.each(function (c) {
    if (c.trackid === selectedSample || !selectedSample) {
      d3.select(this)
        .attr('opacity', 1)
    } else {
      d3.select(this)
        .attr('opacity', 0.15)
    }
  });

  showLinesConnection();
};

const mouseOnSample = (e, idx) => {
  const toolTSM = svg.selectAll('.rect-tooltip-sample');
  const toolTxt = svg.selectAll('.trackid-text');

  toolTSM.each(function (c, i) {
    const rt = d3.select(this);
    if (idx === i) {
      const txtToolTip = d3.select(toolTxt.nodes()[idx]);
      let ms = c.tracklabel;
      ms = ms.replace('\n', ' ');
      txtToolTip.text(ms).call(wrap, 100);
      const bbox = txtToolTip.node().getBBox();
      rt.attr('width', bbox.width + 14)
        .attr('height', bbox.height + 8);
    }
  });
};

const mouseOutSample = e => {
  const toolTSM = svg.selectAll('.rect-tooltip-sample');
  toolTSM.attr('width', 0).attr('height', 0);

  const toolTxt = svg.selectAll('.trackid-text');
  toolTxt.text('');
};

const onClickLoading = (e, d) => {
  selectedLoading = e;

  const arrowSelected = svg.selectAll('.loadings');
  arrowSelected.each(function (c) {
    if (c.label === selectedLoading || !selectedLoading) {
      d3.select(this)
        .attr('opacity', 0.7);
    } else {
      d3.select(this)
        .attr('opacity', 0.15);
    }
  });

  if (!selectedLoading) {
    //loading unselected
    Shiny.setInputValue(
      'arrow_cliked',
      'null',
      { priority: 'event' }
    );
  } else {
    //loading selected
    Shiny.setInputValue(
      'arrow_cliked',
      selectedLoading,
      { priority: 'event' }
    );
  }
};

const mouseOnLoading = (d, i) => {
  // Get the current mouse coordinates
  const [mouseX, mouseY] = d3.mouse(loadingsplot.node());

  const arrowHovered = svg.selectAll('.loadings');

  arrowHovered.each(function (c, j) {
    if (i === j) {
      d3.select(this)
        .attr('stroke', 'orange');
      svg.select(`#arrow-marker-${j} path`)
        .attr('fill', 'orange');
    }
  });

  // Append a text element for the label
  loadingsplot.append('text')
    .attr('class', 'loading-tooltip')
    .attr('x', mouseX + 10)
    .attr('y', mouseY)
    .text(d.label);
  Shiny.setInputValue(
    'arrow_hovered',
    d.label,
    { priority: 'event' }
  );
};

const mouseOutLoading = () => {
  const arrowHovered = svg.selectAll('.loadings');
  arrowHovered.attr('stroke', 'black');
  svg.selectAll('marker path')
    .attr('fill', 'black');
  loadingsplot.selectAll('.loading-tooltip').remove();
  Shiny.setInputValue(
    'arrow_hovered',
    'null',
    { priority: 'event' }
  );
};

// this line reinitialize all the elements of the svg
svg.select('#maingroupPCoA').remove();

const g = svg.append('g').attr('id', 'maingroupPCoA')
  .attr('transform', `translate(${margin.left},${margin.top})`)

// Define scales for x and y axes
const xScale = d3.scaleLinear()
  .domain(d3.extent(data.data_points['Axis.1']))
  .range([0, innerWidth])
  .nice();

const yScale = d3.scaleLinear()
  .domain(d3.extent(data.data_points['Axis.2']))
  .range([innerHeight, 0])
  .nice();

const xLoadingScale = d3.scaleLinear()
  .domain(d3.extent(data.data_loadings['Axis.1']))
  .range([0, innerWidth])
  .nice();

const yLoadingScale = d3.scaleLinear()
  .domain(d3.extent(data.data_loadings['Axis.2']))
  .range([innerHeight, 0])
  .nice();

const xAxis = d3.axisBottom(xScale);
const yAxis = d3.axisLeft(yScale);

// Add x-axis
const xAxisGroup = g.append('g').call(xAxis)
  .attr('transform', `translate(0,${innerHeight})`);

// X-axis legend
xAxisGroup.append('text')
  .attr('class', 'title-XAxis')
  .attr('x', innerWidth / 2)
  .attr('y', 45)
  .text('PC1');

//add y-axis
const yAxisGroup = g.append('g').call(yAxis);

// Y-axis legend
yAxisGroup.append('text')
  .attr('class', 'title-YAxis')
  .attr('x', -innerHeight / 2)
  .attr('y', -35)
  .attr('transform', `rotate(-90)`)
  .attr('text-anchor', 'middle')
  .text('PC2')

// scatter plot area
// Extract 'Axis.1' and 'Axis.2' values from data
const xValues = data.data_points['Axis.1'];
const yValues = data.data_points['Axis.2'];
const clrs = data.data_points['cluster'];
const trackid = data.data_points['track_ID'];
const tracklabel = data.data_points['track_label'];
const data_points = xValues.map((x, i) => ({
  x, y: yValues[i], cluster: clrs[i],
  trackid: trackid[i], tracklabel: tracklabel[i]
}));


const scatterplot = g.append('g');
scatterplot.selectAll('.points')
  .data(data_points)
  .enter()
  .append('circle')
  .attr('class', 'points')
  .attr('cx', d => xScale(d.x))
  .attr('cy', d => yScale(d.y))
  .attr('r', scatterRadius)
  .attr('fill', d => colorScale(d.cluster))
  .on('click', (e, d) => onClickTrackID(e.trackid))//;
  .on('mouseover', (e, i) => mouseOnSample(e, i))
  .on('mouseleave', e => mouseOutSample(e));

//tooltip for tracking samples
const xsepT = 5;
const ysepT = 2;
const frameRect = g.append('g');
frameRect.selectAll('.rect-tooltip-sample')
  .data(data_points)
  .enter()
  .append('rect')
  .attr('class', 'rect-tooltip-sample')
  .attr('rx', 6)
  .attr('ry', 6)
  .attr('x', d => xScale(d.x) + xsepT)
  .attr('y', d => yScale(d.y) + ysepT)
  .attr('fill', '#606060');

const xTxtsep = xsepT + 8;
const yTxtsep = ysepT + 14;
const frameText = g.append('g');
frameText.selectAll('.trackid-text')
  .data(data_points)
  .enter()
  .append('text')
  .attr('class', 'trackid-text')
  .attr('x', d => xScale(d.x) + xTxtsep)
  .attr('y', d => yScale(d.y) + yTxtsep)
  .text('');

// Loadings plot area
const xLoadings = data.data_loadings['Axis.1'];
const yLoadings = data.data_loadings['Axis.2'];
const lblLoadings = data.data_loadings['variable'];
const arrowWidth = 6;
const data_loadings = xLoadings.map((x, i) => ({ x, y: yLoadings[i], label: lblLoadings[i] }));

const loadingsplot = g.append('g');
loadingsplot.selectAll('.loadings')
  .data(data_loadings)
  .enter()
  .append('line')
  .attr('class', 'loadings')
  .attr('x1', xScale(0))
  .attr('y1', yScale(0))
  // .attr('x2', d => xScale(d.x))
  // .attr('y2', d => yScale(d.y))
  .attr('x2', d => xLoadingScale(d.x))
  .attr('y2', d => yLoadingScale(d.y))
  .attr('stroke', 'black')
  .attr('stroke-width', 1.5)
  .attr('marker-end', (d, i) => `url(#arrow-marker-${i})`)
  .attr('opacity', d => (d.label === hoveredBarToArrow ||
    hoveredBarToArrow === 'null' ||
    !hoveredBarToArrow) ? 0.7 : 0.1)
  .on('mouseover', (e, i) => mouseOnLoading(e, i))
  .on('mouseleave', e => mouseOutLoading(e))
  .on('click', (e, i) => onClickLoading((e.label === selectedLoading ? null : e.label), i));

loadingsplot.append('defs').selectAll('marker')
  .data(data_loadings)
  .enter()
  .append('marker')
  .attr('id', (d, i) => `arrow-marker-${i}`)
  .attr('markerWidth', arrowWidth)
  .attr('markerHeight', arrowWidth)
  .attr('refX', arrowWidth / 2)
  .attr('refY', arrowWidth / 2)
  .attr('orient', 'auto')
  .append('path')
  .attr('d', 'M0,0 L0,6 L6,3 Z')
  .attr('fill', 'black');

// Legend area
const legendGroup = g.append('g')
  .attr('transform', `translate(${legendPosition},${margin.top + 50})`);

const title = legendGroup.append('g')
  .attr('class', 'title-text');

title.append('text')
  .attr('x', 10)
  .attr('y', -20)
  .text('Cluster');

legendGroup.selectAll('.legend-bullet')
  .data(lblClusters)
  .enter()
  .append('circle')
  .attr('class', 'legend-bullet')
  .attr('cx', 10)
  .attr('cy', (d, i) => (i * spacingLegend) + 10)
  .attr('r', circleRadiusLegend)
  .attr('fill', d => colorScale(d))
  .on('click', (e, d) => onClickLegend(e === selectedBullet ? null : e));

legendGroup.selectAll('.legend-items')
  .data(colorScale.domain())
  .enter()
  .append('text')
  .attr('class', 'legend-items')
  .text(d => d)
  .attr('x', 25)
  .attr('y', (d, i) => (i * spacingLegend) + 10 + (circleRadiusLegend));

const vizButtonGroup = g.append('g')
  .attr('transform', `translate(${legendPosition},${innerHeight})`);

const trackingButton = vizButtonGroup.append('g')
  .attr('class', 'tracking-toggle-button');

const circleButtonTracking = trackingButton.append('circle')
  .attr('cx', 30)
  .attr('cy', -15)
  .attr('r', 5)
  .attr('fill', 'black')
  .attr('stroke', 'black')
  .attr('cursor', 'pointer')
  .on('click', toggleTrackingOnClick);

trackingButton.append('text')
  .attr('x', 40)
  .attr('y', -10)
  .text('Show Tracking Samples');

const loadingsButton = vizButtonGroup.append('g')
  .attr('class', 'loadings-toggle-button');

const circleButtonLoadings = loadingsButton.append('circle')
  .attr('cx', 30)
  .attr('cy', 5)
  .attr('r', 5)
  .attr('fill', 'black')
  .attr('stroke', 'black')
  .attr('cursor', 'pointer')
  .on('click', toggleArrowOnClick);

loadingsButton.append('text')
  .attr('x', 40)
  .attr('y', 10)
  .text('Show Arrows');

